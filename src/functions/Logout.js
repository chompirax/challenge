import React, {Component} from "react";
import {inject, observer} from "mobx-react";
import store from '../application/store';

class Logout extends Component {
	constructor(props) {
		super(props);
	};
	
	componentDidMount() {
		store.userStore.logout();
		this.props.history.push('/');
	};
	
	render() {
		return (<div>Logout</div>);
	}
}

export default inject('store')(observer(Logout));