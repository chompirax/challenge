import React, {Component} from "react";
import {BrowserRouter as Router, Route} from "react-router-dom";
import {observer, Provider} from "mobx-react";
import store from "./application/store";
import Routes from "./application/routes";
import "antd/dist/antd.css";
import "./assets/css/style.css";
import Dashboard from "./views/Dashboard";

const AppView = observer(
	class App extends Component {
		constructor(props) {
			super(props);
			this.state = {
				routes: null
			};
		}

		componentDidMount() {
			let routes = (
				<div>
					<Route exact path="/" component={Routes.Login}/>
				</div>
			);
			let validate = store.userStore.validateLogin();
			if (validate) {
				routes = (
					<div>
						<Route exact path="/" component={Routes.Login}/>
						<Dashboard>
							<Route path="/agencies" component={Routes.Agencies}/>
							<Route path="/agencies-db" component={Routes.AgenciesDB}/>
							<Route path="/cities" component={Routes.Cities}/>
							<Route path="/logout" component={Routes.Logout}/>
						</Dashboard>
					</div>
				);
			}
			this.setState({routes: routes});
		}

		render() {
			return (
				<Provider store={store}>
					<Router>{this.state.routes}</Router>
				</Provider>
			);
		}
	}
);

export default AppView;
