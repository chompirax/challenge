import React, {Component} from "react";
import {inject, observer} from "mobx-react";
import {PageHeader, Table, Tag, Input, Button, Icon} from 'antd';
import store from '../application/store';
import Highlighter from 'react-highlight-words';



class Agencies extends Component {
	constructor(props) {
		super(props);
		this.state = {
			agencyDBList: [],
			searchText: ''
		};
	};
	
	componentDidMount() {
		this.loadAgencies('');
	};
	
	loadAgencies = e => {
		let agencyDBList = store.agencyDBStore.getAgencyDBList(e);
		agencyDBList.then(() => {
			this.setState({agencyDBList: store.agencyDBStore.agencyDBList});
		});
	};
	
	getColumnSearchProps = dataIndex => ({
		filterDropdown: ({setSelectedKeys, selectedKeys, confirm, clearFilters}) => (
			<div style={{padding: 8}}>
				<Input
					ref={node => {
						this.searchInput = node;
					}}
					placeholder={`Search ${dataIndex}`}
					value={selectedKeys[0]}
					onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
					onPressEnter={() => this.handleSearch(selectedKeys, confirm)}
					style={{width: 188, marginBottom: 8, display: 'block'}}
				/>
				<Button
					type="primary"
					onClick={() => this.handleSearch(selectedKeys, confirm)}
					icon="search"
					size="small"
					style={{width: 90, marginRight: 8}}
				>
					Search
				</Button>
				<Button onClick={() => this.handleReset(clearFilters)} size="small" style={{width: 90}}>
					Reset
				</Button>
			</div>
		),
		filterIcon: filtered => (
			<Icon type="search" style={{color: filtered ? '#1890ff' : undefined}}/>
		),
		onFilter: (value, record) =>
			record[dataIndex]
				.toString()
				.toLowerCase()
				.includes(value.toLowerCase()),
		onFilterDropdownVisibleChange: visible => {
			if (visible) {
				setTimeout(() => this.searchInput.select());
			}
		},
		render: text => (
			<Highlighter
				highlightStyle={{backgroundColor: '#ffc069', padding: 0}}
				searchWords={[this.state.searchText]}
				autoEscape
				textToHighlight={text.toString()}
			/>
		),
	});
	
	handleSearch = (selectedKeys, confirm) => {
		confirm();
		this.setState({searchText: selectedKeys[0]});
	};
	
	handleReset = clearFilters => {
		clearFilters();
		this.setState({searchText: ''});
	};
	
	render() {
		const columns = [
			{
				title: 'Name',
				dataIndex: 'name',
				key: 'name',
				sorter: (a, b) => a.name.length - b.name.length,
				sortDirections: ['descend']
			},
			{
				title: 'City',
				dataIndex: 'city',
				key: 'city',
				sorter: (a, b) => a.city.length - b.city.length,
				sortDirections: ['descend'],
				...this.getColumnSearchProps('city'),
			},
			{
				title: 'State',
				dataIndex: 'state',
				key: 'state',
				sorter: (a, b) => a.state.length - b.state.length,
				sortDirections: ['descend']
			},
			{
				title: 'Status',
				key: 'status',
				dataIndex: 'status',
				sorter: (a, b) => a.status.length - b.status.length,
				sortDirections: ['descend'],
				render: status =>
					status === 'Open' ? (
							<span>
				<Tag color="green">
				{status.toUpperCase()}
				</Tag>
				</span>) :
						(<span>
				<Tag color="red">{status.toUpperCase()}</Tag>
				</span>)
			}
		];
		
		return (
			<>
				<PageHeader title="Agencies from DB"/>
				<Table columns={columns} dataSource={this.state.agencyDBList} rowKey='id'/>
			</>
		);
	}
}

export default inject('store')(observer(Agencies));