import React from "react";
import {Layout, Menu, Icon} from 'antd';
import {Link, withRouter} from 'react-router-dom';
import logo from "../assets/img/logo.png";

const {Header, Sider, Content, Footer} = Layout;

const menus = [
	{path: '/agencies', label: 'Agencies', icon: 'star'},
	{path: '/agencies-db', label: 'Agencies from DB', icon: 'bank'},
	{path: '/cities', label: 'Cities from DB', icon: 'environment'},
	{path: '/logout', label: 'Logout', icon: 'logout'},
];

function Dashboard({children}) {
	return (
		<Layout style={{minHeight: '100vh'}}>
			<Sider
				breakpoint="lg"
				collapsedWidth="0"
			>
				<div className="menu-logo-container">
					<img src={logo} alt="Viamericas" className="menu-logo"/>
				</div>
				<Menu theme="dark" mode="inline" defaultSelectedKeys={['4']}>
					{menus.map(item => (
						<Menu.Item key={item.path}>
							<Link to={item.path} className="menu-link">
								<span className="menu-link"><Icon type={item.icon}/> {item.label}</span>
							</Link>
						</Menu.Item>
					))}
				</Menu>
			</Sider>
			<Layout>
				<Header style={{background: '#fff', padding: 0}}/>
				<Content style={{margin: '24px 16px 0'}}>
					<div style={{padding: 24, background: '#fff', minHeight: 360}}>{children}</div>
				</Content>
				<Footer style={{textAlign: 'center'}}>Viamericas ©2019 Created by Hamilton Ruiz</Footer>
			</Layout>
		</Layout>
	);
}

export default withRouter(Dashboard);
