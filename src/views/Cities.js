import React, {Component} from "react";
import {inject, observer} from "mobx-react";
import store from '../application/store';
import {PageHeader, Table} from "antd";

const columns = [
	{
		title: 'Name',
		dataIndex: 'name',
		key: 'name',
		sorter: (a, b) => a.name.length - b.name.length,
		sortDirections: ['descend']
	},
	{
		title: 'States',
		dataIndex: 'state',
		key: 'state',
		sorter: (a, b) => a.state.length - b.state.length,
		sortDirections: ['descend']
	}
];

class Cities extends Component {
	constructor(props) {
		super(props);
		this.state = {
			cityList: []
		};
	};
	
	componentDidMount() {
		this.loadCities();
	};
	
	loadCities = e => {
		let cityList = store.cityStore.getCityList();
		cityList.then(() => {
			this.setState({cityList: store.cityStore.cityList});
		});
	};
	
	render() {
		return (
			<>
				<PageHeader title="Cities">
					<div className="content padding">This information comes from the database. According to the requirements, the
						agencies reside in these cities. I added a new one, Medellín.
					</div>
				</PageHeader>
				<Table columns={columns} dataSource={this.state.cityList} rowKey='id'/>
			</>
		);
	}
}

export default inject('store')(observer(Cities));