import React, {Component} from "react";
import {Layout, Form, Icon, Input, Button, message} from "antd";
import stores from "../application/store";
import logo from "../assets/img/logo.png";
import {observer} from "mobx-react";

const {Footer, Content} = Layout;

const Login = observer(
	class Login extends Component {
		constructor(props) {
			super(props);
			this.state = {
				loading: false,
				iconLoading: false,
				errorMessage: false
			};
		}
		
		componentDidMount() {
			let validate = stores.userStore.validateLogin();
			if (validate) {
				this.location.reload('/agencies');
			}
		}
		
		handleSubmit = e => {
			e.preventDefault();
			this.props.form.validateFields((err, values) => {
				if (!err) {
					this.enterLoading();
					let login = stores.userStore.loginUser(values);
					login.then(() => {
						if (stores.userStore.logged) {
							this.props.history.push("/agencies");
						} else {
							this.enterLoading();
							message.error(
								"Your access credentials are incorrect, please try again."
							);
						}
					});
				}
			});
		};
		
		enterLoading = () => {
			this.setState({loading: !this.state.loading});
		};
		
		render() {
			const {getFieldDecorator} = this.props.form;
			
			return (
				<Layout style={{minHeight: "100vh"}}>
					<Content>
						<div className="login-box">
							<img src={logo} alt="Viamericas" className="brand-logo"/>
							<h1> Access </h1>
							<section className="login-input-box">
								<Form onSubmit={this.handleSubmit} className="login-form">
									<div className="login-input-content">
										<Form.Item>
											{getFieldDecorator("username", {
												rules: [
													{
														required: true,
														message: "Please input your Username!"
													}
												]
											})(
												<Input
													prefix={
														<Icon
															type="user"
															style={{color: "rgba(0,0,0,.25)"}}
														/>
													}
													type="username"
													placeholder="Please enter your username"
												/>
											)}
										</Form.Item>
									</div>
									<div className="login-input-content">
										<Form.Item>
											{getFieldDecorator("password", {
												rules: [
													{
														required: true,
														message: "Please input your Password!"
													}
												]
											})(
												<Input
													prefix={
														<Icon
															type="lock"
															style={{color: "rgba(0,0,0,.25)"}}
														/>
													}
													type="password"
													placeholder="Please enter your password"
												/>
											)}
										</Form.Item>
									</div>
									<Button
										className="login-button"
										htmlType="submit"
										loading={this.state.loading}
									>
										Login
									</Button>
								</Form>
							</section>
						</div>
					</Content>
					<Footer className="footer-content">
					</Footer>
				</Layout>
			);
		}
	}
);

export default Form.create({name: "authLogin"})(Login);