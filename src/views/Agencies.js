import React, {Component} from "react";
import {inject, observer} from "mobx-react";
import {PageHeader, Table, Tag} from 'antd';
import store from '../application/store';

const columns = [
	{
		title: 'Name',
		dataIndex: 'name',
		key: 'name',
		sorter: (a, b) => a.name.length - b.name.length,
		sortDirections: ['descend']
	},
	{
		title: 'City',
		dataIndex: 'city',
		key: 'city',
		sorter: (a, b) => a.city.length - b.city.length,
		sortDirections: ['descend']
	},
	{
		title: 'State',
		dataIndex: 'state',
		key: 'state',
		sorter: (a, b) => a.state.length - b.state.length,
		sortDirections: ['descend']
	},
	{
		title: 'Status',
		key: 'status',
		dataIndex: 'status',
		sorter: (a, b) => a.status.length - b.status.length,
		sortDirections: ['descend'],
		render: status =>
			status === 'Open' ? (
					<span>
				<Tag color="green">
				{status.toUpperCase()}
				</Tag>
				</span>) :
				(<span>
				<Tag color="red">{status.toUpperCase()}</Tag>
				</span>)
	}
];

class Agencies extends Component {
	constructor(props) {
		super(props);
		this.state = {
			agencyList: []
		};
	};
	
	componentDidMount() {
		this.loadAgencies();
	};
	
	loadAgencies = e => {
		let agencyList = store.agencyStore.getAgencyList();
		agencyList.then(() => {
			this.setState({agencyList: store.agencyStore.agencyList});
		});
	};
	
	render() {
		return (
			<>
				<PageHeader title="Agencies"/>
				<Table columns={columns} dataSource={this.state.agencyList} rowKey='id'/>
			</>
		);
	}
}

export default inject('store')(observer(Agencies));