import {observable, action, decorate} from 'mobx';
import {axiosEvent} from '../plugins/react-axios';

export default class CityStore {
	cityList = [];
	
	async getCityList() {
		let request = {
			method: "GET",
			url: "http://localhost:5000/cities"
		};
		return await axiosEvent(request).then(res => {
			if (res.status === 200) {
				this.cityList = res.data.results;
			} else {
				console.log('oe');
			}
		});
	}
}

decorate(CityStore, {
	cityList: observable,
	getCityList: action
});