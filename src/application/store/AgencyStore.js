import {observable, action, decorate} from 'mobx';
import {axiosEvent} from '../plugins/react-axios';

export default class AgencyStore {
	agencyList = [];

	async getAgencyList() {
		let request = {
			method: "GET",
			url: "https://v45hh4g3q5.execute-api.us-east-1.amazonaws.com/Dev/agencias"
		};
		return await axiosEvent(request).then(res => {
			if (res.status === 200) {
				this.agencyList = res.data;
			}else{
				console.log('oe');
			}
		});
	}
}

decorate(AgencyStore, {
	agencyList: observable,
	getAgencyList: action
});