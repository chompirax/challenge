import {observable, action, decorate} from 'mobx';
import {axiosEvent} from '../plugins/react-axios';

export default class UserStore {

	username = null;
	token = null;
	logged = false;

	async loginUser(values) {
		let request = {
			method: 'POST',
			url: 'https://v45hh4g3q5.execute-api.us-east-1.amazonaws.com/Dev/authentication',
			data: values
		};
		return await axiosEvent(request)
		.then(res => {
			if (res.status === 200) {
				if (res.data) {
					this.username = values.username;
					this.logged = true;
					this.token = res.data.id_token;
					let storage = {
						username: this.username,
						token: this.token
					};
					localStorage.setItem('authViamericas', JSON.stringify(storage))
				}
			}
		})
		.catch(e => {
			console.log('error')
		})
	}

	logout() {
		localStorage.removeItem('authViamericas');
		this.username = null;
		this.token = null;
		this.logged = false;
	}

	validateLogin() {
		let validate = localStorage.getItem('authViamericas');
		if (validate) {
			let auth = JSON.parse(validate);
			this.logged = true;
			this.token = auth.token;
			return true
		}
		return false
	}
}
decorate(UserStore, {
	email: observable,
	token: observable,
	logged: observable,
	loginUser: action,
	logout: action,
	validateLogin: action
});
