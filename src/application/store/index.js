import UserStore from './UserStore';
import AgencyStore from './AgencyStore';
import AgencyDBStore from './AgencyDBStore';
import CityStore from './CityStore';

export default {
	userStore: new UserStore(),
	agencyStore: new AgencyStore(),
	agencyDBStore: new AgencyDBStore(),
	cityStore: new CityStore(),
}