import {observable, action, decorate} from 'mobx';
import {axiosEvent} from '../plugins/react-axios';

export default class AgencyDBStore {
	agencyDBList = [];
	
	async getAgencyDBList(e) {
		let request = {
			method: "POST",
			url: "http://localhost:5000/agencies",
			data: {"city": e}
		};
		return await axiosEvent(request).then(res => {
			if (res.status === 200) {
				this.agencyDBList = res.data.results;
			} else {
				console.log('error');
			}
		});
	}
}

decorate(AgencyDBStore, {
	agencyDBList: observable,
	getAgencyDBList: action
});