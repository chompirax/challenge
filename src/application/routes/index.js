import Login from '../../views/Login';
import Agencies from '../../views/Agencies';
import Cities from '../../views/Cities';
import AgenciesDB from '../../views/AgenciesDB';
import Logout from '../../functions/Logout';

export default {
	Login: Login,
	Agencies: Agencies,
	AgenciesDB: AgenciesDB,
	Cities: Cities,
	Logout: Logout
}
