import axios from "axios";
import store from "../store";


axios.defaults.baseURL = "";
axios.defaults.headers.common = {
	"Content-Type": "application/json",
	'Accept': 'application/json'
};
export const axiosEvent = async values => {
	if (store.userStore.token) {
		axios.defaults.headers.common["Authorization"] = `Bearer ${
			store.userStore.token
			}`;
	}
	switch (values.method) {
		case "POST":
			return await axios.post(values.url, values.data);
		case "GET":
			return await axios.get(values.url);
		default:
			break;
	}
};