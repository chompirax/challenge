Este proyecto se creó con [Create React App](https://github.com/facebook/create-react-app).

## Instrucciones

* Descargar el proyecto desde el repositorio **https://chompirax@bitbucket.org/chompirax/challenge.git**, o clonando el mismo **git clone https://chompirax@bitbucket.org/chompirax/challenge.git**
* Crear una base de datos con nombre "viamericas_challenge" desde el archivo .sql que se encuentra en la raíz del proyecto (**mysql_viamericas.sql**)
* Estando en la raíz del proyecto ejecutar el comando **npm install** para instalar los paquetes del proyecto (FRONT)
* En la misma carpeta server ejecutar el comando **npm start** para subir el servicio, este funcionará por defecto en el puerto **5000**.Luego de esto ubicarse en la carpeta server y realizar el mismo procedimiento (**npm install**)
*  Si se desea correo el server en modo "dev", asegurarse de tener instalado el paquete de **nodemon** en forma global. Con esto funcionando se puede ejecutar el comando **npm run dev**
* Aparte de esto volviendo a la raíz del proyecto ejecutar el comando **npm start** para subir el FRONT
* Si se desea se puede correr el comando **npm run build**
* En el archivo ".env" que se encuentra en la carpeta server se pueden modificar los datos de acceso a la base de datos

## Notas
* El front tiene 3 vistas principales a parte del login, la primera de ellas es el listado de agencias que arroja desde la url indicada en la documentación.
* Las otras 2 vistas están directamente relacionadas a la base de datos, por tanto el server debe estar arriba y la base de datos funcionando.

## LIbrería utilizadas
* Se utilizó en UI, la librería de Ant Design, https://ant.design/
* Se utilizaron además las siguientes librerías para react:
*  axios
*  mobx (Para la estore)
*  mobx-react
*  react
*  react-axios
*  react-dom
*  react-highlight-words
*  react-router-dom
*  react-scripts
* Se utilizaron las siguientes librerías para el server (Backend) en node:
*  body-parser
*  cors
*  dotenv
*  eslint
*  express
*  mysql

* **Dependencias de desarrollo**
*  babel-cli
*  abel-core
*  babel-plugin-inline-dotenv
*  babel-preset-env