import express from "express";
import con from '../config/mysql'

require('../config/index');
const app = express();


app.get('/cities', function (req, res) {
	con.query('SELECT a.*, b.name as state FROM cities as a INNER JOIN states as b ON a.state_id=b.id', function (error, results, fields) {
		if (error) {
			return res.status(400).json({
				ok: false,
				error
			});
		}
		res.json({
			ok: true,
			results: results
		});
	});
});

app.get('/city', function (req, res) {
	let body = req.body;
	console.log(body.city);
	con.query(`SELECT a.id, a.name, b.name as state
	FROM cities a
	INNER JOIN states b ON a.state_id=b.id
	WHERE a.id=? `, [body.city], function (error, results) {
		if (error) {
			return res.status(400).json({
				ok: false,
				error
			});
		}
		res.json({
			ok: true,
			results: results
		});
	});
});

app.post('/agencies', function (req, res) {
	let body = req.body;
	console.log(body.city);
	con.query(`SELECT a.id, a.name, b.name as city, c.name as state, d.name as status
	FROM agencies a
	INNER JOIN cities b ON a.city_id=b.id AND b.name LIKE CONCAT('%', ?,  '%')
	INNER JOIN states c ON b.state_id=c.id
	INNER JOIN status d ON a.status_id=d.id`, [body.city], function (error, results) {
		if (error) {
			return res.status(400).json({
				ok: false,
				error
			});
		}
		res.json({
			ok: true,
			results: results
		});
	});
});

module.exports = app;
