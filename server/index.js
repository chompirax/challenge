import express from "express";
import cors from "cors";

const app = express();
const bodyParser = require("body-parser");
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
app.use(cors());
app.use(require("./routes/index"));

app.listen(process.env.PORT, () => {
	console.log("Now listening on port " + process.env.PORT)
});